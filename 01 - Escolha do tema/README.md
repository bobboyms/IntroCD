# Projeto: Índice de suicídios cometido por idosos no Brasil

## Equipe: Fortress_Pattern

## Descrição: Analisar o índice de suicídio no Brasil, média anual, destacando a média em cada estado. Porém o objetivo é identificar o índice de suicídio entre os idosos.

## Membros: (liste os membros informando nome, RA, login gitlab, curso, universidade. siga o exemplo abaixo)

Thiago Luiz Rodrigues 01, RA: a2296322, @bobboyms, PPGCA (Mestrado Profissional), UTFPR

Gerson Jovino Peres 02, RA: a2436345, @gersonperes, PPGCA (Mestrado Profissional), UTFPR
